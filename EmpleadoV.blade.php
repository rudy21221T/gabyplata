
<body style="width: 100%; font-size: 100%; background-color: #58606D;">
	<div class="container">
<div align="center">
			<label style="color: white; font-weight: 900; font-size: 200%; font-family: arial">EMPLEADOS</label>
		</div>

		<!-- Button trigger modal -->
		<button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#exampleModal" style="margin-top: 10px;">
			Ingresar Empleado
		</button>
		<div class="table-responsive">
			<table border="1" class="table table-dark table-hover" style="width: 100%;" id="laravel_datatable">
				<thead>
					<tr>
						<td>N°</td>
						<td>Nombre</td>
						<td>Apellido</td>
						<td>Dui</td>
						<td>Fecha de nacimiento</td>
						<td>Fecha de contratacion</td>
						<td>Direccion</td>
						<td>Telefono</td>
						<td>Correo</td>
						<td>Sexo</td>
						<td>ELIMINAR</td>
					</tr>
				</thead>
				<tbody>
					<?php $n = 1; foreach ($c as $v) { ?>
						<tr>
							<td><?php echo $n; ?></td>
							<td><?php echo $v->nombre; ?></td>
							<td><?php echo $v->apellido; ?></td>
							<td><?php echo $v->dui; ?></td>
							<td><?php echo $v->fecha_nacimiento; ?></td>
							<td><?php echo $v->fecha_contratacion; ?></td>
							<td><?php echo $v->direccion; ?></td>
							<td><?php echo $v->telefono; ?></td>
							<td><?php echo $v->correo; ?></td>
							<td><?php echo $v->id_sexo; ?></td>
							<td><a href="" class="btn btn-danger">ELIMINAR</a></td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>


		<!-- Modal -->
		<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Nuevo Empleado</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<form action="" method="POST">
							<div>
								<label>Nombre</label>
								<input type="text" name="nombre" class="form-control" placeholder="Ingrese el nombre de el Empleado..">
							</div>
							<div>
								<label>Apeliido</label>
								<input type="text" name="apellido" class="form-control" placeholder="Ingrese el apellido de el Empleado..">
							</div>
							<div>
								<label>DUI</label>
								<input type="text" name="dui" class="form-control" id="dui">
							</div>
							<div>
								<label>Fecha de nacimiento</label>
								<input type="date" name="fechan" class="form-control">
							</div>
							<div>
								<label>Fecha de contratacion</label>
								<input type="date" name="fechac" class="form-control">
							</div>
							<div>
								<label>Direccion</label>
								<input type="text" name="direccion" class="form-control" placeholder="Ingrese la direccion...">
							</div>
							<div>
								<label>Telefono</label>
								<input type="text" name="telefono" class="form-control" id="telefono">
							</div>
							<div>
								<label>Correo</label>
								<input type="text" name="correo" class="form-control" placeholder="Ingrese el correro electronico">
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
							<input type="submit" name="Guardar" value="Guardar" class="btn btn-primary">
						</div>
					</form>
				</div>
			</div>
		</div>

	</div>

	<script type="text/javascript">
		$(document).ready(function(){
        //FORMATO DE MASCARAS
        $('#telefono').mask('0000-0000', {placeholder: '0000-0000'});
        $('#dui').mask('00000000-0', {placeholder: '00000000-0'});
    });


		$(document).ready( function () {
			$('#laravel_datatable').DataTable({
				processing: true,
				serverSide: true,
				ajax: "{{ url('empleado-list') }}",
				columns: [
				{ data: 'dui', name: 'dui' },
			
				]
			});
		});
	</script>