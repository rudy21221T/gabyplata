<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ClienteM;
use Illuminate\Support\Facades\DB;
class Cliente extends Controller
{
	public function index()
	{

		$c=ClienteM::all();
		$title = "Cliente || Farmaceutica";
		echo view('templates/header',compact('title'));
		echo view('templates/navbar');
		echo view('ClienteV',compact('c'));
		echo view('templates/footer');
	}

	public function Pedido($id){
		$data['c']= DB::table('clientes as c')->join('ciudad as ci','ci.id_ciudad','=','c.id_ciudad')->where('id_cliente', '=', $id)->get();
		$title = "Pedido || Farmaceutica";
		echo view('templates/header',compact('title'));
		echo view('templates/navbar');
		echo view('PedidoV',$data)->render();
		echo view('templates/footer');
	}

	public function eliminar(){
		//accedemos a la base y su tabla indicamos con un where la busqueda y luego llamamos al metodo de eliminar.
		DB::table('clientes')->where('id_cliente', '=', $id)->delete();
		return redirect()->Route('index');
	}
}
